class Config(object):
    DEBUG = True
    PATH = r'..\instance'


class Testing(Config):
    TESTING = True
