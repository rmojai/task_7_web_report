import pytest
from flaskr.race import races
from flask import request


@pytest.fixture()
def client():
    client = races.test_client()
    return client


def test_common_statistic(client):
    response = client.get('/report/')
    assert response.status_code == 200


def test_drivers_list(client):
    response = client.get('/report/drivers/')
    assert response.status_code == 200


def test_driver_info(client):
    response = client.get("/report/drivers/?driver_id=BHS")
    assert b"Brendon Hartley" in response.data


def test_request_drivers_desc():
    with races.test_request_context("/report/drivers/?order=desc#"):
        assert request.args['order'] == 'desc'


def test_driver_desc_order(client):
    response = client.get("/report/drivers/?order=desc#")
    assert b'<li>Carlos Sai' \
           b'nz</li>\n            </td>\n            <td><a href="/report/drivers/?drive' \
           b'r_id=CSR">CSR</a>\n            </td>\n        </tr>\n        \n        <' \
           b'tr>\n            <td>\n                <li>Brendon Hartley</li>\n          ' \
           b'  </td>\n            <td><a href="/report/drivers/?driver_id=BHS">BHS</a>\n' \
           in response.data


def test_request_drivers_asc():
    with races.test_request_context("/report/drivers/?order=asc#"):
        assert request.args['order'] == 'asc'


def test_driver_asc_order(client):
    response = client.get("/report/drivers/?order=asc#")
    assert b'<li>Stoffel Vandoor' \
           b'ne</li>\n            </td>\n            <td><a href="/report/drivers/?drive' \
           b'r_id=SVM">SVM</a>\n            </td>\n        </tr>\n        \n        <' \
           b'tr>\n            <td>\n                <li>Valtteri Bottas</li>\n          ' \
           b'  </td>\n            <td><a href="/report/drivers/?driver_id=VBM">VBM</a>\n' \
           in response.data


def test_request_report_desc():
    with races.test_request_context("/report/?order=desc#"):
        assert request.args['order'] == 'desc'


def test_report_desc_order(client):
    response = client.get("/report/?order=desc#")
    assert b'<li>Valtteri Bottas</li' \
           b'>\n            </td>\n            <td>MERCEDES</td>\n            <td>0:01:1' \
           b'2.434000</td>\n        </tr>\n        \n        <tr>\n            <td>\n ' \
           b'               <li>Sebastian Vettel</li>\n            </td>\n            <' \
           b'td>FERRARI</td>\n            <td>0:01:04.415000</td>\n' \
           in response.data


def test_request_report_asc():
    with races.test_request_context("/report/?order=asc#"):
        assert request.args['order'] == 'asc'


def test_report_asc_order(client):
    response = client.get("/report/?order=asc#")
    assert b'<li>Esteban Ocon</li>\n            </t' \
           b'd>\n            <td>FORCE INDIA MERCEDES</td>\n            <td>Error time<' \
           b'/td>\n        </tr>\n        \n        <tr>\n            <td>\n          ' \
           b'      <li>Sergey Sirotkin</li>\n            </td>\n            <td>WILLIAM' \
           b'S MERCEDES</td>\n            <td>Error time</td>\n' \
           in response.data


if __name__ == '__main__':
    pytest.main()
