from flaskr import create_app

races = create_app()
races.config.from_object('race_config.Config')

if __name__ == '__main__':
    races.run()
